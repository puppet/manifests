node 'api.insomnia247.nl' {
  include role::manager_api
}

node 'belle.pve.insomnia247.nl' {
  include role::proxmox_ve
}

node 'borg.pve.insomnia247.nl' {
  include role::borgserver
}

node 'chat2.insomnia247.nl' {
  include role::webchat
}

node 'ci01.insomnia247.nl' {
  include role::docker::standalone
}

node 'ci02.insomnia247.nl' {
  include role::docker::standalone_managed_workloads
}

node 'docker01.oci.insomnia247.nl' {
  include role::docker::standalone_managed_workloads
}

node 'fay.pve.insomnia247.nl' {
  include role::proxmox_ve
}

node 'elastic.insomnia247.nl' {
  include role::base
  include profile::domaincerts
}

node 'firewall03.insomnia247.nl' {
  include role::router
}

node 'freya.pve.insomnia247.nl' {
  include role::proxmox_ve
}

node 'gateway02.insomnia247.nl' {
  include role::vpn::gateway
  include role::suricata_sensor
}

node 'git.insomnia247.nl' {
  include role::gitlab
}

node 'git2.insomnia247.nl' {
  include role::gitlab
}

node 'jetson.insomnia247.nl' {
  include role::docker::standalone_managed_workloads
}

node /^lydia[67]\.insomnia247\.nl$/ {
  include role::lydia6
}

node 'n01.leaf.irc.insomnia247.nl' {
  include role::irc::hub
  include role::irc::leaf
  include role::irc::anope
  include role::heartbeat
}

node 'n02.leaf.irc.insomnia247.nl' {
  include role::irc::leaf
  include role::irc::nanobot
  include role::irc::nanobot_api
}

node 'instance-20220624-1807.vcn06241812.oraclevcn.com' {
  include role::cacheproxy
}

node 'puppet.insomnia247.nl' {
  include role::puppetmaster
}

node 'pxe.pve.insomnia247.nl' {
  include role::docker::standalone_managed_workloads
}

node 'rundeck-host.insomnia247.nl' {
  include role::docker::standalone_managed_workloads
}

node 'vpnrouter.pve.insomnia247.nl' {
  include role::vpn::router
  include role::suricata_sensor
}

node 'vps87769.vps.ovh.ca' {
  include role::irc::leaf
}

node 'vps297715.ovh.net' {
  include role::irc::leaf
}

node 'vault-host.insomnia247.nl' {
  include role::docker::standalone_managed_workloads
}